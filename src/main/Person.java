package main;

public class Person {
    private String name;
    private String secondName;
    private int age;

    public Person(String name, String secondName, int age) {
        this.name = name;
        this.secondName = secondName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return age;
    }
    String toJS(Person person) {
        String name = person.getName();
        String secondName = person.getSecondName();
        String age = Integer.toString(person.getAge());
        String result = "\"name\":\"" + name + "\",\n" +
                "\"secondName\":\"" + secondName + "\",\n" +
                "\"age\": " + age;
        return result;
    }

    Person fromJS(String string) {
        String[] mass = string.split("\\\"");
        String[] newMass = new String[mass.length];
        for (int i = 0; i < mass.length; i++) {
            if (i == 0 || i % 2 == 0) {
            } else {
                newMass[i] = mass[i];
            }
        }
        int cheek = 0;
        for (int i = 0; i < newMass.length; i++) {
            if (newMass[i] == null) {
                cheek++;
            }
        }
        String[] arr = new String[newMass.length - cheek];
        for (int i = 0, j = 1; i < arr.length; i++, j += 2) {
            arr[i] = mass[j];
        }
        int age = 0;
        String name = null;
        String secondName = null;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                if (arr[i].equals("age")) {
                    age = Integer.parseInt(arr[j + 1]);
                    break;
                }
                if (arr[i].equals("name")) {
                    name = arr[j + 1];
                    break;
                }
                if (arr[i].equals("secondName")) {
                    secondName = arr[j + 1];
                    break;
                }
            }
        }
        Person person = new Person(name, secondName, age);
        return person;
    }
}
