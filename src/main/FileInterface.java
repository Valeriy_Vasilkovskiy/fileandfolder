package main;

import java.io.File;
import java.io.IOException;

public interface FileInterface {
    File createFile(String path, String fileName);
    File createDir(String path, String name);
    File renameFile(String path, String lastName, String newName);
    void deleteDir(String name);
    void deleteFile(String path);
    void readFile(String path) throws IOException;
    void writeFile(String[] text, String path) throws IOException;
    void writeFile(String text, String path) throws IOException;
}
