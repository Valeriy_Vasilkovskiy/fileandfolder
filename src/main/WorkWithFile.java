package main;
import java.io.*;

public class WorkWithFile implements FileInterface {

    @Override
    public File createFile(String path, String fileName) {
        File file = new File(path, fileName);
        try {
            if (!file.exists()) {
                System.out.println("Файл \"" + fileName + "\" создан");
                file.createNewFile();
            } else {
                System.out.println("Файл \"" + fileName + "\" уже существует");
                return file;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public File createDir(String path, String name) {
        File dir = new File(path, name);
        try {
            if (dir.mkdir()) {
                System.out.println("Папка создана");
            } else {
                System.out.println("Папка не создана, возможно она уже существует");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dir;
    }

    @Override
    public File renameFile(String path, String lastName, String newName) {
        File file = new File(path, lastName);
        if (file.getName().equals(newName)) {
            System.out.println("Файл \"" + file.getName() + "\" уже существует");
            return file;
        } else {
            System.out.println("\"" + file.getName() + "\" был переименован в \"" + newName + "\"");
            File newFile = new File(path, newName);
            file.renameTo(newFile);
            return file;
        }
    }

    @Override
    public void deleteDir(String path) {
        File file = new File(path);
        if (file.delete()) {
            System.out.println("\"" + path + "\" была удалена c корневой директории проекта");
        } else
            System.out.println("\"" + path + "\" не была найдена в корневой директории проекта или не является пустой");
    }

    @Override
    public void deleteFile(String path) {
        File file = new File(path);
        if (file.delete()) {
            System.out.println("\"" + file.getName() + "\" был удалён");
        } else {
            System.out.println("\"" + file.getName() + "\" не было обнаружено");
        }
    }

    @Override
    public void readFile(String path) throws IOException {
        File file = new File(path);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while (bufferedReader.ready()) {
            System.out.print(bufferedReader.readLine());
        }
        System.out.println();
    }

    @Override
    public void writeFile(String[] text, String path) throws IOException {
        File file = new File(path);
        FileWriter writer = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        for (int i = 0; i < text.length; i++) {
            bufferedWriter.write(i);
        }
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    @Override
    public void writeFile(String text, String path) throws IOException {
//        Scanner scanner = new Scanner(System.in);
        File file = new File(path);
        FileWriter writer = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        String[]  txt = text.split("\\n");
        boolean is_Empty = true;
//        bufferedWriter.write(scanner.nextLine());
        char[] str = text.toCharArray();
        for (int j = 0; j < str.length; j++) {
            if (str[j] == ',') {
                for (int i = 0; i < txt.length; i++) {
                    if (i == txt.length - 1) {
                        bufferedWriter.write(txt[i]);
                        is_Empty = false;
                        break;
                    } else {
                        txt[i] = txt[i] + "\n";
                        bufferedWriter.write(txt[i]);
                        bufferedWriter.newLine();
                    }
                }
                break;
            }
        }
        if(is_Empty){
            bufferedWriter.write(text);
        }
        bufferedWriter.flush();
        bufferedWriter.close();
    }
}
