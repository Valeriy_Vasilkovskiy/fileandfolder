package main;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        //C:\Users\User\Desktop
        WorkWithFile work = new WorkWithFile();
        Person person = new Person("Karl", "Mercury", 23);
        work.createDir("C://Users/User/Desktop","For Java File");
        work.createFile("C://Users/User/Desktop/For Java File","Первый.txt");
        work.renameFile("C://Users/User/Desktop/For Java File", "Первый.txt", "Второй.txt");
        work.deleteDir("C://Users/User/Desktop/For Java File");
        work.deleteFile("C://Users/User/Desktop/For Java File/Первый.txt");
        work.writeFile(person.toJS(person),"C://Users/User/Desktop/For Java File/Второй.txt");
        work.readFile("C://Users/User/Desktop/For Java File/Второй.txt");

    }
}
